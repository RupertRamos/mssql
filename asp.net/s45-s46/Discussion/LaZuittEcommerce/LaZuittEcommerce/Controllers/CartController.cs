
using LaZuittEcommerce.Data;
using LaZuittEcommerce.Models;
using LaZuittEcommerce.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace LaZuittEcommerce.Controllers;

public class CartController : Controller
{
    private readonly LaZuittContext _context;

    public CartController(LaZuittContext context)
    {
        _context = context;
    }

    public IActionResult Index()
    {
        // Create new instance for our CartItem class model
        List<CartItem> cart = HttpContext.Session.GetJson<List<CartItem>>("Cart") ?? new List<CartItem>();

        // Create new instance for our CartViewModel class model
        CartViewModel cartVM = new()
        {
            CartItems = cart,
            TotalAmount = cart.Sum(x => x.Quantity * x.Price)
        };

        // Returns the CartViewModel as View to our Cart/Index.cshtml
        return View(cartVM);
    }

    public async Task<IActionResult> Add(int id)
    {
        // Awaits the Add to Cart click from the user
        Course course = await _context.Course.FindAsync(id);

        // Created a new instance of List<> that acts as our session
        List<CartItem> cart = HttpContext.Session.GetJson<List<CartItem>>("Cart") ?? new List<CartItem>();

        // Search the course based on the passed id parameter
        CartItem cartItem = cart.Where(c => c.CourseId == id).FirstOrDefault();

        // Check if item already exists in our cart
        bool cartItemExists = cart.Any(i => i.CourseId == id);

        // Initial item input
        if (cartItem == null)
        {
            cart.Add(new CartItem(course));
        }

        // Prevents item duplication
        else if (cartItemExists == true)
        {
            TempData.Keep();
            return RedirectToAction("Index", "Course");
        }

        // Initialize the "Cart" session to the value of cart 
        HttpContext.Session.SetJson("Cart", cart);

        // Initialize our TempData["Count"] to the number of items in the cart
        TempData["Count"] = cart.Count();

        // To retain all TempData values
        TempData.Keep();

        // Redirect to Course/Index.cshtml
        return RedirectToAction("Index", "Course");
    }

    public IActionResult Remove(int id)
    {
        // Retrieve our cart session data
        List<CartItem> cart = HttpContext.Session.GetJson<List<CartItem>>("Cart");

        // Remove a record based on the Id
        cart.RemoveAll(p => p.CourseId == id);

        // Remove all session if no item exists
        if (cart.Count == 0)
        {
            HttpContext.Session.Remove("Cart");
        }

        // Set our session to existing cart items
        else
        {
            HttpContext.Session.SetJson("Cart", cart);
        }

        // Reassign TempData["Count"] new count value and keep the TempData value
        TempData["Count"] = cart.Count();
        TempData.Keep();

        // Redirect to Cart/Index.cshtml or same page
        return RedirectToAction("Index");
    }

}

