﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FinalTestChar.Models
{
    public class Enroll
    {
        [Key]
        public int Id { get; set; }

        //[ForeignKey("StudentId")]
        public int StudentId { get; set; }
        public virtual Student Student { get; set; }

        //[ForeignKey("CourseId")]
        public int CourseId { get; set; }
        public virtual Course Course { get; set; }

        [Display(Name = "Enroll Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime EnrollDate { get; set; }

        [Display(Name = "Billing Address")]
        public string BillingAddress { get; set; }
    }
}

