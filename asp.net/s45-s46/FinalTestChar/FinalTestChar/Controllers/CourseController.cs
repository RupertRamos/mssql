using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using FinalTestChar.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Text.Json;
using Newtonsoft.Json.Linq;
using System.Xml.Linq;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace FinalTestChar.Controllers
{
    public class CourseController : Controller
    {
        private readonly LaZuittContext _context;
        public bool isStudentAdded = false;

        public CourseController(LaZuittContext context)
        {
            _context = context;
        }

        public int[] idList = { };
        private int[] fromPostId = { };

        public int[] pubDummy = { 2, 1, 3 };

        // Retreive and display Courses on the Index page
        public async Task<IActionResult> Index()
        {
            //TempData["count"] = idList.Count();
            TempData.Keep();

            return View(await _context.Course.ToListAsync());

        }

        // Retrieve course by Id
        public ActionResult AddToCart(int id)
        {
            var query = _context.Course.Where(i => i.Id == id).FirstOrDefault();
            return View(query);
        }

        // Redirect to Course/Index.cshtml after clicking Add To Cart button

        [HttpPost]
        public ActionResult AddToCart()
        {
            return RedirectToAction("Index");
        }

        // Display items at the checkout page based on Id's save in the local storage

        public ActionResult EmptyCart()
        {
            return View();
        }

        public ActionResult Checkout()
        {
            return View();
        }


        [HttpGet]
        [Route("[controller]/CheckoutCart")]
        //[ActionName("Checkout")]
        public ActionResult _MyCart(string values, bool btnClicked)
        {
            //Console.WriteLine("this is from get result: " + JsonConvert.SerializeObject(values)); // "1, 2, 3"

            Console.WriteLine("bool" + btnClicked);

            if (btnClicked == true)
            {
                Console.WriteLine("im clicked");

                //var tblEnroll = from e in _context.Enroll select e;

                //foreach(var item in tblEnroll.ToList())
                //{
                //    Enroll enroll = new Enroll
                //    {
                //        StudentId = 1,
                //        CourseId = 3,
                //        EnrollDate = DateTime.Today,
                //        BillingAddress = "lorem"
                //    };

                //    Console.WriteLine("dotpppp");

                //    _context.Enroll.Add(enroll);
                    
                //};
                    
                //return View();
            }

            else
            {
                Console.WriteLine("Not clicked");
            }

            int[] dummyValues = { 2, 3, 1 };

            string value = String.Join(", ", values);
            string newValue = new string(value); // 1, 2, 3

            int[] ids = newValue.Select(i => i - '0').ToArray(); // string to int[]


            //Console.WriteLine("converted: " + newValue);


            var course = _context.Course.Where(x => ids.Contains(x.Id)).ToList();
            //return View(course);

            var res = JsonConvert.SerializeObject(course);
            //Console.WriteLine(res);
            var param = new { cart = course };

            return Json(res);
        }



        //Student Entry
        [HttpPost]
        [ValidateAntiForgeryToken]

        [Route("[controller]/Checkout")]
        public async Task<IActionResult> Checkout([Bind("Id,FirstName,LastName,Email")] Student student)
        {
            if (ModelState.IsValid)
            {
                _context.Add(student);
                await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }
            return View(student);
        }

        [HttpPost]
        [Route("[controller]/ButtonClick")]
        public ActionResult ButtonClick(string button, bool clicked)
        {
            bool isClick = false;
            if (button == "Checkout")
            {
                isClick = true;
                var tblEnroll = from c in _context.Course select c;

                Enroll enroll = new Enroll
                {
                    StudentId = 1,
                    CourseId = 2,
                    EnrollDate = DateTime.Today,
                    BillingAddress = "lorem"
                };

                //Enroll enroll = new Enroll();
                //enroll.StudentId = 1;
                //enroll.CourseId = 2;
                //enroll.EnrollDate = DateTime.Now;
                //enroll.BillingAddress = "lorem";


                Console.WriteLine("dotpppp");

                //_context.Add(enroll);
                _context.Add(enroll);
                _context.SaveChanges();




            }
            return RedirectToAction("_MyCart", new { btnClicked = isClick });
        }
    }
}
