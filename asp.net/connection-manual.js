

/*
CONNECTING an SQLSERVER to an ASP.NET app:

1. Under the View tab, open the Server Explorer panel/window.

2. Select Connect to Database button.

3. Add connection details:
	Data source: Microsoft SQL Server (SqlClient)
	Server name: DESKTOP-BI7SNTQ\SQLEXPRESS
	Authentication: Windows Authentication
	Database name: LaZuitt

4. Click the test connection to confirm if connection would succeed or not.

5. Once successful click ok. A new data connections will appear on the server explorer window.

6. Right click on it and go to properties.

7. Copy the connection string.
	
	Sample
	Connection String:	Data Source=DESKTOP-BI7SNTQ\\SQLEXPRESS;Initial Catalog=LaZuitt;Integrated Security=True

8. Go to appsettings.json and add the database connection string under the "ConnectionStrings" 

9. Add the TrustServerCertificate=Yes into the connection string to prevent the error related to the server certificates.
*/

// Sample:
  "ConnectionStrings": {
    "DefaultConnection": "Data Source=DESKTOP-BI7SNTQ\\SQLEXPRESS;Initial Catalog=LaZuitt;Integrated Security=True;TrustServerCertificate=Yes"
  }

// 10. Go to Program.cs and refactor the AddDbContext services:

  // Import the Microsoft.EntityFrameworkCore.SqlSercer v 7.0.9 or the version that matches your other dependencies.

  builder.Services.AddDbContext<LaZuittContext>(options =>
{
    options.UseSqlServer(builder.Configuration["ConnectionStrings:DefaultConnection"]);
});


// Note: Change the values of the Price to a lower range (4 digits only).













/*===========================================================================================================*/



// [SECTION] Adding Services for the creating and retrieval of user object.

// 1. Under the discussion, create a Services Folder. Inside it, create a UserRepositories folder.


// 2.Inside the Userrepositories folder create an IUserRepository interface and add the following code:


// Code snippet Start:

  using discussion.Models;

  namespace discussion.Services.UserRepositories
  {
      public interface IUserRepository
      {

          Task<Student> GetByEmail(string email);

          Task<Student> Create(Student student);


      }
  }

// Code snipper End

// 3. Create a DatabaseUserRepository class on the same folder and add the implementation of the IUserRepository methods in it.


  // Code snippet start:
  using discussion.Data;
  using discussion.Models;
  using Microsoft.EntityFrameworkCore;

  namespace discussion.Services.UserRepositories
  {
      public class DatabaseUserRepository : IUserRepository
      {
          //Inject DbContext here
          private readonly LaZuittContext _context;

          //Create the class constructor
          public DatabaseUserRepository(LaZuittContext context)
          {
              _context = context;
          }

          public async Task<Student> Create(Student student)
          {
              _context.Student.Add(student);
              await _context.SaveChangesAsync();
              return student;
          }

          public async Task<Student> GetByEmail(string email)
          {
              return await _context.Student.FirstOrDefaultAsync(s => s.Email == email);
          }

      }
  }

  // Code snippet end:

// 4. Add the IUserRepository interface and DatabaseUserRepository class in the Program.cs as a Singleton service.



// Code snippet start

// Top:
  using discussion.Services.UserRepositories;




// Before the AddDbContext service:

//Added the UserRepository service as a single entity.
builder.Services.AddScoped<IUserRepository, DatabaseUserRepository>();


// Code snippet end