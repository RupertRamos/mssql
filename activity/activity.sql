
-- Records to insert:
INSERT INTO rooms VALUES ('Manila','Single', 'A room good for 1 person', 1, 5000, 1)
INSERT INTO rooms VALUES ('Manila','Double', 'A room good for 2 persons', 2, 8000, 1)
INSERT INTO rooms VALUES ('Makati','Queen Deluxe', 'A room good for 3 persons', 3, 12000, 1)
INSERT INTO rooms VALUES ('Makati','Deluxe Solo', 'A deluxe room good for 1 person', 1, 20000, 1)
INSERT INTO rooms VALUES ('Makati','King Deluxe', 'A deluxe room good for 4 persons', 4, 36000, 0)



-- Activity Solution:

-- 1. Create a HotelDB database

		CREATE DATABASE	HotelDB

-- 2. Create the rooms table:

		CREATE TABLE rooms (
			Branch varchar(50) NOT NULL,
			Room_Type varchar(50) NOT NULL,
			Description varchar(100) NOT NULL,
			Accommodation int NOT NULL,
			Price int NOT NULL,
			IsAvailable tinyint NOT NULL
		)

-- 3. Items 5-10:

	SELECT * FROM rooms WHERE Branch='Makati' AND Price > 15000

	SELECT Branch, Room_Type, Price FROM rooms WHERE IsAvailable=1

	SELECT Branch, Room_Type FROM rooms WHERE Accommodation<=2

	UPDATE rooms SET Price=10000, Description='a room that can accomodate 2 persons only' WHERE Room_Type='Double'

	UPDATE rooms SET IsAvailable=0 WHERE Branch='Manila'

	DELETE FROM rooms WHERE Room_Type='King Deluxe'