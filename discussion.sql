-- Create Database:
-- CREATE DATABASE <database_name>
CREATE DATABASE CourseBookingDB


-- Drop Database:
-- DROP DATABASE <database_name>
DROP DATABASE EmployeeDB


-- ALTER DATABASE:
	ALTER DATABASE CourseBookingDB
	ADD FILE (
	NAME = TestDB,
	FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\TestDB.MDF',
	SIZE = 3 MB, MAXSIZE = 8 MB, FILEGROWTH = 2 MB
	)
