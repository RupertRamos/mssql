
-- References:
/*
	SQL Sub Languages: https://www.geeksforgeeks.org/sql-ddl-dql-dml-dcl-tcl-commands/

*/



-- Code Discussion

-- 1. Open the Microsoft SQL Server Management System.
/*
	-- Make sure the Object Explorer is open. Click on View and select Object Explorer or press F8.
	-- Click Connect dropdown and select Database Engine.
	-- For the server name we will just use the user/admin created upon installation of the program.
	-- For Authentication: Windows Authentication.
		-- This is also what is most preferred by companies.
	-- Once done with the options/configurations, click on connect.
	-- A Database Engine will be created on the Object Explorer panel. 

	-- Browse the sub folders under the newly created Database Engine instance.

	--Databases
		-- System Databases
			-- master - which contains all the information, systems data, meta data about the instance.
	-- Security
		-- Logins - allows us to create Login credentials for server level access. Can also grant user access at database level.

*/


-- [SECTION] DDL - Database Definition Language

/*
	CREATE: This command is used to create the database or its objects (like table, index, function, views, store procedure, and triggers).
	DROP: This command is used to delete objects from the database.
	ALTER: This is used to alter the structure of the database.
	TRUNCATE: This is used to remove all records from a table, including all spaces allocated for the records are removed.
	COMMENT: This is used to add comments to the data dictionary.
	RENAME: This is used to rename an object existing in the database.
*/

-- 2. Database creation.

	-- 2.a. Using the GUI
		-- Right click on Databases and select new database
		-- A new window will pop up:
			-- Database name: EmployeeDB
			-- Owner: <default>

		-- This will create a new database instance under the databases folder.
		-- This will also create a primary file and log file on the specified path which is usually the DATA folder.

	-- 2.b Using the query window
		-- Click on New Query to open a new query window.

		-- command:
		-- CREATE DATABASE <database_name>
			CREATE DATABASE CourseBookingDB

-- 3. Drop Database command.

	-- 3.a Using the GUI
		-- Right click on the database you want to delete. On this example we will create a StudentDB and delete it using the GUI.
		-- After creating the StudentDB, right click on it and select the Delete option.
		-- A new window will pop up and will ask you to confirm the deletion of the database object.
		-- Click ok and refresh the Databases folder or click the refresh button on the Object Explorer.

	-- 3.b Using the Query Window
		-- command:
		-- DROP DATABASE <database_name>

			DROP DATABASE EmployeeDB

	-- F5 - to execute the query when on query window; refresh on the Object Explorer window.


-- 4. Alter Database

	-- 4.a Add a file in the database

	-- command:
	ALTER DATABASE CourseBookingDB
	ADD FILE (
	NAME = TestDB,
	FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\TestDB.MDF',
	SIZE = 3 MB, MAXSIZE = 8 MB, FILEGROWTH = 2 MB
	)

	-- 4.b Modify a file in the database
	ALTER DATABASE CourseBookingDB
	MODIFY FILE (
	NAME = TestDB,
	SIZE = 5 MB, MAXSIZE = Unlimited
	)


	-- 4.c Modify the filename in the database

		-- Right click on CourseBookingDB and select Rename and change the database name to CourseBooking.
		-- Then using the query below revert it back to CourseBookingDB.

	ALTER DATABASE CourseBooking
	MODIFY NAME = CourseBookingDB

	-- 4.d Remove file from database
	ALTER DATABASE CourseBookingDB
	REMOVE FILE TestDB

-- 5. Use/Select Database

	-- To change and use a specific database
	-- command:
	USE CourseBooking

	-- To determine your selected database
	SELECT DB_NAME()


-- 6. Proceed in Creating Tables for the CourseBookingDB

	-- 6.a Using the GUI create a Courses Table

		-- Go to CourseBookingDB, click the + sign to expand the folders inside.
		-- Go to Tables and right click to add a new table.
		-- Provide the following specifications to the students:

			/*
			Column Name 		Data type			Allow Nulls
			Course_id			int
			Course_name			varchar(50)				yes
			Description			varchar(50)				yes
			Price				int 					yes
			*/

		-- save the table and name it as "courses". 
		-- check the newly create table under the Tables folder.

	-- 6.b Using SQL Syntax:

	CREATE TABLE users(
		id int PRIMARY KEY NOT NULL,
		Firstname varchar(50) NOT NULL,
		Lastname varchar(50) NOT NULL,
		Email varchar(50) NOT NULL,
		Password varchar(50) NOT NULL,
	)


-- 7. Demonstrate ALTER command for altering tables


	-- 7.a Add a new column on an existing table

	-- command:
		ALTER TABLE users
		ADD MobileNo int

	-- 7.b Altering the column name

	-- command:
		ALTER TABLE users
		ALTER COLUMN Password varchar(20)

	-- 7.c Dropping a table column

	-- command:
		ALTER TABLE users
		DROP COLUMN MobileNo

	-- 7.d Renaming the table

	-- command:
		SP_RENAME 'users', 'students'

	-- change it back:
		SP_RENAME 'students', 'users'

-- 8. Truncate a Table

	/*
		Truncate -This is used to remove all records from a table, including all spaces allocated for the records are removed.
	*/

	-- 8.a Right click on the users table and select edit top 200 rows.

	-- 8.b Insert/Add test cases for the users table. At least 3 would be enough.

	-- 8.c After adding some users in the users table. Demonstrate the Truncate command.

	-- command:

		TRUNCATE TABLE users

-- 9. Droping a table

	-- 9.a Create a dummy table for this example first and add some data.

	-- command:
		CREATE TABLE employees (
			id int PRIMARY KEY NOT NULL,
			Firstname varchar(50) NOT NULL,
			Lastname varchar(50) NOT NULL,
			Email varchar(50) NOT NULL,
		)

	-- command:
		DROP TABLE employees

	-- Note: Remind the trainees to be cautious in executing the DROP command as it delete both the structure and the data.

-- [SECTION] DML - Database Manipulation Language
/*
	INSERT: It is used to insert data into a table.
	UPDATE: It is used to update existing data within a table.
	DELETE: It is used to delete records from a database table.
	LOCK: Table control concurrency.
	CALL: Call a PL/SQL or JAVA subprogram.
	EXPLAIN PLAN: It describes the access path to data.
*/

-- 10. Demonstrate the INSERT command.

	-- 10.a Insert a single row

	-- command:
		INSERT INTO dbo.users (id, Firstname,Lastname,Email,Password)
		     VALUES (1,'Jenny','Doe','jennydoe@gmail.com','jenny123')
		INSERT INTO dbo.users (id,Firstname,Lastname,Email,Password)
		     VALUES (2,'Julie','Smith','juliesmith@gmail.com','julie456')
		INSERT INTO dbo.users (id,Firstname,Lastname,Email,Password)
		     VALUES (3,'Jacky','Petterson','jackypetterson@gmail.com','jacky789')

	-- command: to insert in all columns
		INSERT INTO dbo.users VALUES (8,'Jeffrey','Coleman','jeff@gmail.com','jeff123')



	-- 10.b Right click on the users table and click select top 1000 to view the updated table entries.
 

-- 11. Insert some data in the courses table.

	-- 11.a First insert these data in the courses table:

	-- command:
		INSERT INTO dbo.courses VALUES (1, 'ASP.NET', '.NET Framework for web development', 10000)
		INSERT INTO dbo.courses VALUES (2, 'C#', 'Language developed by Microsoft', 8000)
		INSERT INTO dbo.courses VALUES (3, 'Angular', 'Frontend Framework developed by Google', 12000)
		INSERT INTO dbo.courses VALUES (4, 'XUnit Test', '.NET Framework Testing', 15000)
		INSERT INTO dbo.courses VALUES (5, 'Docker', 'Deploy apps using containers', 18000)

-- 12. Demonstrate the SELECT command.


	-- 12.a Select data by specifying the column names

	-- command:
		SELECT Firstname, Lastname, Email FROM users

	-- 12.b Select data from all the columns.

	-- command:
		SELECT * FROM users

	-- 12.c Where Clause

	-- command:
		SELECT Firstname, Lastname, Email FROM users WHERE Lastname='Doe'

		SELECT	Course_name, Description FROM courses WHERE Price >=12000

		SELECT	Course_name, Description FROM courses WHERE Price >=12000 AND Course_name='Docker'
		SELECT	Course_name, Description FROM courses WHERE Price >=12000 OR Course_name='Docker'

-- 13. Demonstrate the Update Command

	-- command:
		UPDATE courses SET Price=20000 WHERE Course_name='ASP.NET'

		UPDATE courses SET Description='Course not available', Price=0 WHERE Price < 15000

	-- Note: emphasize on the trainees the importance of using the WHERE clause. Without is all the values in the spefied columns will be updated.

-- 14. Demonstrate the Delete Command

	-- command:
		DELETE FROM courses WHERE Price=0

	-- command:
		-- Delete all records from the table. Always use WHERE clause.
		DELETE FROM courses

	

	/*
		TRUNCATE TABLE <table_name> - does not need the where clause to remove all the records
		
		DROP TABLE <table_name> - deletes the data and table structure.
	*/

-- 15. Demonstrate Table Join

	-- 15.a Delete all the current tables and the data in each tables.

	-- 15.b Create the courses and users tables again using the following query syntax:

		CREATE TABLE users(
			id int NOT NULL,
			Firstname varchar(50) NOT NULL,
			Lastname varchar(50) NOT NULL,
			Email varchar(50) NOT NULL,
			Password varchar(50) NOT NULL,
			CONSTRAINT pk_users PRIMARY KEY (id)
		)

		CREATE TABLE courses(
			id int NOT NULL,
			Course_name varchar(50) NOT NULL,
			Description varchar(50) NOT NULL,
			Price int NOT NULL,
			CONSTRAINT pk_courses PRIMARY KEY (id)
		)


		CREATE TABLE user_enroll(
			id int NOT NULL,
			user_id int
			course_id int,
			CONSTRAINT pk_user_enroll PRIMARY KEY (id),
			FOREIGN KEY (user_id) REFERENCES users (id),
			FOREIGN KEY (course_id) REFERENCES courses (id)
		)


		-- Insert Users Data:
		INSERT INTO dbo.users (id, Firstname,Lastname,Email,Password)
		     VALUES (1,'Jenny','Doe','jennydoe@gmail.com','jenny123')
		INSERT INTO dbo.users (id,Firstname,Lastname,Email,Password)
		     VALUES (2,'Julie','Smith','juliesmith@gmail.com','julie456')
		INSERT INTO dbo.users (id,Firstname,Lastname,Email,Password)
		     VALUES (3,'Jacky','Petterson','jackypetterson@gmail.com','jacky789')

		-- Insert Courses data:
		INSERT INTO dbo.courses VALUES (1, 'ASP.NET', '.NET Framework for web development', 10000)
		INSERT INTO dbo.courses VALUES (2, 'C#', 'Language developed by Microsoft', 8000)
		INSERT INTO dbo.courses VALUES (3, 'Angular', 'Frontend Framework developed by Google', 12000)
		INSERT INTO dbo.courses VALUES (4, 'XUnit Test', '.NET Framework Testing', 15000)
		INSERT INTO dbo.courses VALUES (5, 'Docker', 'Deploy apps using containers', 18000)

		-- Insert User_Enroll Data
		INSERT INTO dbo.user_enroll VALUES (1, 2, 1)
		INSERT INTO dbo.user_enroll VALUES (2, 2, 4)
		INSERT INTO dbo.user_enroll VALUES (3, 2, 5)
		INSERT INTO dbo.user_enroll VALUES (4, 1, 1)
		INSERT INTO dbo.user_enroll VALUES (5, 1, 3)
		INSERT INTO dbo.user_enroll VALUES (6, 3, 5)


	-- 15.c. Demonstrate JOIN

	-- command:
		SELECT * FROM user_enroll ue
			INNER JOIN users u ON ue.user_id=u.id
			INNER JOIN courses c ON c.id=ue.course_id

		SELECT Firstname,Lastname,Email,Course_name, ue.id FROM user_enroll ue
			INNER JOIN users u ON ue.user_id=u.id
			INNER JOIN courses c ON c.id=ue.course_id



-- Activity

/*
	Instructions:
	1. Create an activity.sql file and write here the necessary sql commands for creating database, tables, updating, retrieving and deleting records.
	2. Create a HotelDB database.
	3. Inside the HotelDB, creates a rooms table with the following specifications:

		Branch - varchar(50)
		Room_Type - varchar(50) 
		Description - varchar(100)
		Accommodation - int 
		Price - int
		IsAvailable - tinyint

		*note: all fields are not nullable.
	4. Insert the following data to the rooms tables:

*/

INSERT INTO rooms VALUES ('Manila','Single', 'A room good for 1 person', 1, 5000, 1)
INSERT INTO rooms VALUES ('Manila','Double', 'A room good for 2 persons', 2, 8000, 1)
INSERT INTO rooms VALUES ('Makati','Queen Deluxe', 'A room good for 3 persons', 3, 12000, 1)
INSERT INTO rooms VALUES ('Makati','Deluxe Solo', 'A deluxe room good for 1 person', 1, 20000, 1)
INSERT INTO rooms VALUES ('Makati','King Deluxe', 'A deluxe room good for 4 persons', 4, 36000, 0)

/*
	5. Retrieve all columns from rooms table where the branch name is 'Makati' and the price is greater than 15000.

	6. Retrieve the branch, room_type, and price of records from rooms table that are available.

	7. Retrieve the branch and room_type from the rooms table which can accomodate less than or equal to 2 persons.

	8. Update the record from the rooms table where the room_type is 'Double'. Set the price to 10000 and the description to 'a room that can accomodate 2 persons only'.

	9. Update the records from the Manila Branch. Change the value of IsAvailable to make it unavailable.

	10. Delete the record from the rooms table where the room_type is 'King Deluxe'

*/

-- Activity Solution:

-- 1. Create a HotelDB database

		CREATE DATABASE	HotelDB

-- 2. Create the rooms table:

		CREATE TABLE rooms (
			Branch varchar(50) NOT NULL,
			Room_Type varchar(50) NOT NULL,
			Description varchar(100) NOT NULL,
			Accommodation int NOT NULL,
			Price int NOT NULL,
			IsAvailable tinyint NOT NULL
		)

-- 3. Items 5-10:

	SELECT * FROM rooms WHERE Branch='Makati' AND Price > 15000

	SELECT Branch, Room_Type, Price FROM rooms WHERE IsAvailable=1

	SELECT Branch, Room_Type FROM rooms WHERE Accommodation<=2

	UPDATE rooms SET Price=10000, Description='a room that can accomodate 2 persons only' WHERE Room_Type='Double'

	UPDATE rooms SET IsAvailable=0 WHERE Branch='Manila'

	DELETE FROM rooms WHERE Room_Type='King Deluxe'